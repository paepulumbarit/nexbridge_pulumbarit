-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2019 at 11:30 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nxbridge`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_06_01_085215_create_themes_table', 1),
(2, '2019_06_01_092620_create_users_table', 1),
(3, '2019_06_01_100822_create_posts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `created_at`, `updated_at`, `user_id`) VALUES
(2, 'Yes', 'or yes?<br />\r\n<br />\r\n:D', '2019-05-22 09:14:28', '2019-05-27 09:35:34', 2),
(3, 'I love myself', 'Another day another rant!', '2019-06-01 09:17:25', '2019-06-01 09:17:25', 3),
(4, 'Heavy Traffic', 'Extremely heavy traffic from Ortigas to Munoz. OMG.', '2019-06-01 09:21:42', '2019-06-01 09:21:42', 3),
(5, 'Sore throat', 'Why do I have to experience sore throat today T_T <br />\r\nGot a lot of performance this afternoon...<br />\r\n<br />\r\nHopefully can still hit high notes!', '2019-05-31 09:22:59', '2019-06-01 09:22:59', 1),
(6, 'Innocent rapper?', 'No one in our group wants to rap because no one has experience...<br />\r\nShould I get out of my own comfort zone?<br />\r\nIt doesn\'t fit my image though.<br />\r\n<br />\r\nT-T', '2019-06-01 09:26:08', '2019-06-01 09:26:44', 4);

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `image`) VALUES
(1, 'Nature', 'images/themes_images/nature.jpg'),
(2, 'Polygon', 'images/themes_images/polygon.jpg'),
(3, 'Techie', 'images/themes_images/techie.jpg'),
(4, 'Minimalist', 'images/themes_images/minimalist.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'images/user_images/default-avatar.jpg',
  `birthday` date NOT NULL,
  `theme_id` bigint(20) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `last_name`, `first_name`, `username`, `password`, `image`, `birthday`, `theme_id`) VALUES
(1, 'Yuju', 'Yuju', 'ChoiYuna', '$2y$10$Djd3T7V5z5EU2muL65DbwutsH3kFKvPJWp0I8FgsJwRs/MzNhIrly', 'images/user_images/2019-06-01171119.jpg', '1997-10-15', 3),
(2, 'Yoo', 'Jeong-yeon', 'Jeongyeon', '$2y$10$LKVdIkJBJE9rFtR6pkaftenRJ1veBRyELxqMli2WF6pTg.KGNTkcK', 'images/user_images/2019-06-01171345.jpg', '1996-11-01', 1),
(3, 'Hwang', 'Ye-ji', 'Dalla0526', '$2y$10$JrJcelAeKigolmSy68ZJeuPdI2dBELxo8t0eNSTIihWxBlbY/noMG', 'images/user_images/2019-06-01171650.jpg', '2000-05-26', 4),
(4, 'Hyewon', 'Kang', 'MinamiMom', '$2y$10$Uo1pjqKUO6oDuVE5WTS2x.rYlnCXAcczad89t433isTbW7M8aXLLi', 'images/user_images/2019-06-01172519.jpg', '1999-07-05', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD KEY `users_theme_id_foreign` (`theme_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
