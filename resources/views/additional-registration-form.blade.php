<form method="post" action="/register">
    <div class="row w-100 mx-auto p-2">
        <p>Please fill up the remaining info to finish registration.</p>
        {{ csrf_field() }}
        <div class="col-12 col-md-6 p-1">
            <div class="form-group my-0">
                <label class="my-1" for="txtUsername">Username</label>
                <input type="text" id="txtUsername" class="form-control" name="username" readonly>
            </div>
        </div>
        <div class="col-12 col-md-6 p-1">
            <div class="form-group my-0">
                <label class="my-1" for="txtPassword">Password</label>
                <input type="password" id="txtPassword" class="form-control" name="password" readonly>
            </div>
        </div>
        <div class="col-12 col-md-6 p-1">
            <div class="form-group my-0">
                <label class="my-1" for="txtLastName">Last Name</label>
                <input type="text" id="txtLastName" name="last_name" class="form-control txt-name">
                <p class="d-none text-danger text-left my-0"></p>
            </div>
        </div>
        <div class="col-12 col-md-6 p-1">
            <div class="form-group my-0">
                <label class="my-1" for="txtFirstName">First Name</label>
                <input type="text" id="txtFirstName" name="first_name" class="form-control txt-name">
                <p class="d-none text-danger text-left my-0"></p>
            </div>
        </div>
        <div class="col-12 p-1">
            <p class="my-0">Birthday</p>
        </div>
        <div class="col-12 col-md-6 p-1">
            <div class="form-group my-0">
                <label class="my-1" for="selectMonth">Month</label>
                <select id="selectMonth" class="form-control custom-select" name="month">
                    <option value="1">January</option>
                    <option value="2">February</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
            </div>
        </div>
        <div class="col-5 col-md-2 p-1">
            <label class="my-1" for="selectDay">Day</label>
            <select id="selectDay" class="form-control custom-select" name="day"></select>
        </div>
        <div class="col-7 col-md-4 p-1">
            <label class="my-1" for="selectYear">Year</label>
            <select id="selectYear" class="form-control custom-select" name="year">
                @for($i = 1950; $i <= 2010; $i++)
                    <option value="{{ $i }}">{{ $i }}</option>
                @endfor
            </select>
        </div>
        <div class="col-12 p-1">
            <button id="btnReg" class="btn btn-success form-control my-1">Register</button>
        </div>
    </div>
</form>

<script type="text/javascript" src="{{ URL::asset('js/register.js') }}"></script>