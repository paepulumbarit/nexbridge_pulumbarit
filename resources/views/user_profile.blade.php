@extends('partials.template')

@section('title')
    {{ $user->first_name }} {{ $user->last_name }}'s Rant Page
@endsection

@section("content")
    <div id="profile-banner" class="row w-100 mx-auto p-4" style="background-image: url({{ URL::asset($user->theme->image) }});">
        <div class="col-12 mx-auto text-center">
            <div class="row">
                <div
                    @if(Auth::user()->id == $user->id)
                    id="avatarDiv"
                    @endif
                    class="mx-auto text-center">
                    <img src="{{ URL::asset($user->image) }}" id="profilePic" class="d-inline d-lg-block rounded-circle mx-auto border m-4">
                    @if(Auth::user()->id == $user->id)
                        <div class="middle">
                            <label for="file-input"><div><i class="fas fa-camera-retro fa-5x text-white"></i></div></label>
                            <input id="file-input" type="file" class="d-none">
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 text-center text-white banner-text">
            <h1>{{ $user->first_name }} {{ $user->last_name }}</h1>
        </div>
        <div class="col-12 text-center text-white banner-text">
            <h3><i class="fas fa-birthday-cake mr-3"></i> {{ date('F d, Y', strtotime($user->birthday)) }}<i class="fas fa-birthday-cake ml-3"></i></h3>
        </div>
        @if(Auth::user()->id == $user->id)
            <div class="col-12 mt-2 text-right p-0">
                <a href="/update" class="text-white">
                    Edit Profile
                </a> |
                <a id="change-theme-btn" class="pr-2 text-white">
                    Change Theme
                </a>
            </div>
        @endif
    </div>

    <div class="row w-100 mx-auto my-4">
        <div class="d-none d-lg-block col-2"></div>
        <div class="col-12 col-lg-8 p-0">
            <h3 class="text-uppercase">
                @if(Auth::user()->id == $user->id)
                    My
                @else
                    {{ $user->first_name }}'s
                @endif
                Rants
            </h3>

            @if(Auth::user()->id == $user->id)
                <div class="bg-white p-4 border border-secondary my-4 mb-4m-lg-0">
                    <form id="form-new-post">
                        {{ csrf_field() }}
                        <input type="text" id="txtTitle" name="title" class="form-control mb-4 post-inputs" placeholder="Title">
                        <textarea id="txtBody" name="body" class="form-control post-inputs" rows="4"
                        @if(Auth::user()->id == $user->id)
                            placeholder="Tell the world what's frustrating you right now."
                        @else
                            placeholder="Rant on {{ $user->first_name }}'s profile."
                        @endif></textarea>
                        <div class="text-right">
                            <button type="button" id="btnPost" class="btn btn-danger mt-4 px-5"><i class="far fa-angry px-2"></i><strong>Rant!!</strong></button>
                        </div>
                    </form>
                </div>
            @endif

            <div id="posts-container">
                @if($posts->count())
                    @foreach($posts as $post)
                        <div class="post w-100 bg-white border border-secondary p-4 my-3">
                            <div class="row">
                                @if($post->user->id == Auth::user()->id)
                                    <div class="col-8 col-md-10">
                                @else
                                    <div class="col-12">
                                @endif
                                    <h4 class="post-titles">{{ $post->title }}</h4>
                                </div>
                                @if($post->user->id == Auth::user()->id)
                                    <div class="col-2 col-md-1 text-warning text-right">
                                        <i class="fas fa-pencil-alt fa-2x post-options edit-post-btn"
                                            data-toggle="tooltip"
                                            data-id="{{ $post->id }}"
                                            data-placement="top"
                                            title="Edit rant"></i>
                                    </div>
                                    <div class="col-2 col-md-1 text-danger text-left">
                                        <i class="fas fa-times fa-2x post-options del-post-btn"
                                            data-toggle="tooltip"
                                            data-id="{{ $post->id }}"
                                            data-placement="top"
                                            title="Delete rant"></i>
                                    </div>
                                @endif
                                <div class="col-12">
                                    <p class="text-justify post-bodies p-2">{!!$post->body !!}</p>
                                    <h6 class="text-secondary p-2">
                                        {{ date_format($post->created_at, 'F d, Y') }}
                                        ({{ Carbon::createFromTimeStamp(strtotime($post->created_at))->diffForHumans()}})
                                        @if($post->created_at != $post->updated_at)
                                            | Edited {{ Carbon::createFromTimeStamp(strtotime($post->updated_at))->diffForHumans()}}
                                        @endif
                                    </h6>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p id="no-posts">
                        @if(Auth::user()->id == $user->id)
                            You haven't
                        @else
                            {{ $user->first_name }} hasn't
                        @endif
                        ranted yet.
                    </p>
                @endif
            </div>
        </div>
    </div>
@endsection