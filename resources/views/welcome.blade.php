<!DOCTYPE html>
<html>
<head>
    <title>iRant</title>
    <link
        rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <link
        href="https://fonts.googleapis.com/css?family=KoHo|Patrick+Hand&display=swap"
        rel="stylesheet">
    <link
        rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
        crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/home.css') }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    @include("partials.modal")
    <div id="forms-container" class="mx-auto row w-100">
        <div class="col-1 col-lg-2"></div>
        <div id="login-reg-form" class="col-10 col-lg-8 text-center">
            <h1 id="site-name"><i class="far fa-angry px-2"></i>iRant</h1>
            <div class="row">
                <div class="col-12 col-lg-5 mx-auto">
                    <h2>Login</h2>
                    @if (session("invalidCredentials"))
                        <strong class="text-danger"><p class="my-1 text-justify">{{ session("invalidCredentials") }}</p></strong>
                    @endif
                    <form method="post" action="/login" class="text-justify">
                        {{ csrf_field() }}
                        <div class="form-group my-0">
                            <label class="my-1" for="txt-login-username">Username</label>
                            <input
                                type="text"
                                id="txt-login-username"
                                class="form-control login-field"
                                name="login_username"
                                value="{{ old('login_username') }}">
                            <strong class="text-danger"><p class="d-none text-left my-0">Please enter your username</p></strong>
                        </div>
                        <div class="form-group my-0">
                            <label class="my-1" for="txt-login-pass">Password</label>
                            <input
                                type="password"
                                id="txt-login-pass"
                                class="form-control login-field"
                                name="login_password">
                            <strong class="text-danger"><p class="d-none text-left my-0">Please enter your password</p></strong>
                        </div>
                        <input type="submit" id="btn-login" class="btn btn-success form-control my-3" value="Log in">
                    </form>
                </div>
                <div class="d-none d-lg-block col-0 col-lg-2 text-center">
                    <div id="line"></div>
                    <div id="wordwrapper">
                        <div id="word">OR</div>
                    </div>
                </div>
                <div class="col-12 col-lg-5 text-center">
                    <h2>Register</h2>
                    <form id="form-register" class="text-justify">
                        {{ csrf_field() }}
                        <div class="form-group my-0">
                            <label class="my-1" for="txt-reg-user">Username</label>
                            <input type="text" id="txt-reg-user" class="form-control reg-field">
                            <strong class="text-danger"><p class="d-none text-left my-0"></p></strong>
                        </div>
                        <div class="form-group my-0">
                            <label class="my-1" for="txt-reg-pass">Password</label>
                            <input type="password" id="txt-reg-pass" class="form-control reg-field">
                            <strong class="text-danger"><p class="d-none text-left my-0"></p></strong>
                        </div>
                        <div class="form-group my-0">
                            <label class="my-1" for="txt-reg-conf-pass">Confirm Password</label>
                            <input type="password" id="txt-reg-conf-pass" class="form-control reg-field">
                            <strong class="text-danger"><p class="d-none text-left my-0"></p></strong>
                        </div>
                        <button type="button" id="btn-reg" class="btn btn-success form-control my-3">Continue</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-1 col-lg-2"></div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous">
    </script>
    <script
        src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous">
    </script>
    <script type="text/javascript" src="{{ URL::asset('js/login-register.js') }}"></script>
</body>
</html>