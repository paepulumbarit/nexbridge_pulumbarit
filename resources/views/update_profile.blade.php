@extends('partials.template')

@section("title")
    Update Profile
@endsection

@section("content")
    <div class="row w-100 mx-auto">
        <div class="d-none d-lg-block col-1"></div>
        <div class="col-12 col-lg-5 bg-white p-5 border">
            <h3>Login Credentials</h3>
            <strong><p class="d-none text-danger text-left my-0" id="cred-warning">No changes to be saved.</p></strong>
            <form id="login-cred-form">
                {{ csrf_field() }}
                <label for="txtUsername">Username</label>
                <input type="text" id="txtUsername" name="username" class="cred-field form-control" value="{{ $user->username }}"></input>
                <p class="d-none text-danger text-left my-0"></p>

                <label for="txtPassword">Password</label>
                <input type="password" id="txtPassword" name="password" class="cred-field form-control"></input>
                <p class="d-none text-danger text-left my-0"></p>

                <label for="txtConfPass">Confirm password</label>
                <input type="password" id="txtConfPass" class="cred-field form-control"></input>
                <p class="d-none text-danger text-left my-0"></p>
                <div>
                    <button id="saveCredChanges" type="button" class="w-100 btn btn-success mt-3">Save Changes</button>
                </div>
            </form>
        </div>
        <div class="col-12 col-lg-5 bg-white p-5 border">
            <h3>Profile</h3>
            <strong><p class="d-none text-danger text-left my-0" id="prof-warning">No changes to be saved.</p></strong>
            <form id="profile-form">
                {{ csrf_field() }}
                <label for="txtLastName">Last Name</label>
                <input type="text" id="txtLastName" name="last_name" class="name-field form-control" value="{{ $user->last_name }}">
                <p class="d-none text-danger text-left my-0"></p>

                <label for="txtFirstName">First Name</label>
                <input type="text" id="txtFirstName" name="first_name" class="name-field form-control" value="{{ $user->first_name }}">
                <p class="d-none text-danger text-left my-0"></p>

                <label for="selectMonth">Birthday</label>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <select id="selectMonth" class="form-control custom-select" name="month" data-month="{{ explode("-", $user->birthday)[1] }}">
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </div>
                    <div class="col-5 col-md-2 mt-3 pl-3 p-md-0 mt-md-0">
                        <select id="selectDay" class="form-control custom-select" name="day" data-day="{{ explode("-", $user->birthday)[2] }}"></select>
                    </div>
                    <div class="col-7 col-md-4 mt-3 mt-md-0">
                        <select id="selectYear" class="form-control custom-select" name="year">
                            @for($i = 1950; $i <= 2010; $i++)
                                <option value="{{ $i }}"
                                @if ($i == explode("-", $user->birthday)[0])
                                    selected
                                @endif
                                >{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="col-12">
                        <input type="button" id="saveProfileChanges" value="Save Changes" class="btn btn-success w-100 mt-3">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
