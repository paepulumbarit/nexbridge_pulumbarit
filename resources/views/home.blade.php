@extends('partials.template')

@section('title')
    Home
@endsection

@section("content")
    <div class="row w-100 mx-auto">
        <div class="bg-white col-12 col-lg-3 p-3 border border-secondary">
            <div class="row">
                <div class="col-12 text-center">
                    <img src="{{ URL::asset(Auth::user()->image) }}" class="d-inline d-lg-block rounded-circle mx-auto border m-4">
                </div>
                <div class="col-12">
                    <p class="text-center inline-block pt-3 pt-lg-0 my-lg-1">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
                </div>
            </div>
        </div>
        <div class="d-none d-lg-block col-1"></div>
        <div class="bg-white col-12 col-lg-8 p-4 border border-secondary my-4 m-lg-0">
            <form id="form-new-post">
                {{ csrf_field() }}
                <input type="text" id="txtTitle" name="title" class="form-control mb-4 post-inputs" placeholder="Title">
                <textarea id="txtBody" name="body" class="form-control post-inputs" rows="4" placeholder="Tell the world what's frustrating you right now."></textarea>
                <div class="text-right">
                    <button type="button" id="btnPost" class="btn btn-danger mt-4 px-5"><i class="far fa-angry px-2"></i><strong>Rant!!</strong></button>
                </div>
            </form>
        </div>
    </div>

    <div class="row w-100 mx-auto my-4">
        <div class="d-none d-lg-block col-4"></div>
        <div class="col-12 col-lg-8 p-0">
            <h3 class="text-uppercase">Latest Rants</h3>

            <div id="posts-container">
                @if($posts->count())
                    @foreach($posts as $post)
                        <div class="post w-100 bg-white border border-secondary p-4 my-3">
                            <div class="row">
                                 @if($post->user->id == Auth::user()->id)
                                    <div class="col-8 col-md-10">
                                @else
                                    <div class="col-12">
                                @endif
                                    <h4 class="post-titles">{{ $post->title }}</h4>
                                </div>
                                @if($post->user->id == Auth::user()->id)
                                    <div class="col-2 col-md-1 text-warning text-right">
                                        <i class="fas fa-pencil-alt fa-2x post-options edit-post-btn"
                                            data-toggle="tooltip"
                                            data-id="{{ $post->id }}"
                                            data-placement="top"
                                            title="Edit rant"></i>
                                    </div>
                                    <div class="col-2 col-md-1 text-danger text-left">
                                        <i class="fas fa-times fa-2x post-options del-post-btn"
                                            data-toggle="tooltip"
                                            data-id="{{ $post->id }}"
                                            data-placement="top"
                                            title="Delete rant"></i>
                                    </div>
                                @endif
                                <div class="col-12">
                                    <p class="text-justify post-bodies p-2 ">{!! $post->body !!}</p>
                                    <h6 class="text-secondary pt-2 px-2">
                                        Rant by: <a href="/{{ $post->user->username }}">{{ $post->user->first_name }} {{ $post->user->last_name }}</a>
                                        @if ($post->user->id == Auth::user()->id)
                                            (You)
                                        @endif
                                    </h6>
                                    <h6 class="text-secondary px-2">
                                        {{ date_format($post->created_at, 'F d, Y') }}
                                        ({{ Carbon::createFromTimeStamp(strtotime($post->created_at))->diffForHumans()}})
                                        @if($post->created_at != $post->updated_at)
                                            | Edited {{ Carbon::createFromTimeStamp(strtotime($post->updated_at))->diffForHumans()}}
                                        @endif
                                    </h6>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p id="no-posts">Great, no one is ranting!</p>
                @endif
            </div>
        </div>
    </div>
@endsection