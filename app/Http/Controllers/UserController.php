<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function find(Request $request) {
        $user = User::where('username', ($request->username))->get();

        return $user->count();
    }

    public function create(Request $request) {
        $username = $request->username;
        $password = $request->password;

        $user = new User();
        $user->username = $username;
        $user->password = Hash::make($password);
        $user->last_name = ucwords($request->last_name);
        $user->first_name = ucwords($request->first_name);

        $birthday = $request->year . "-" . $request->month . "-" . $request->day;

        $user->birthday = $birthday;

        $user->save();

        $userdata = array(
            "username" => $username,
            "password" => $password
        );

        if (Auth::attempt($userdata)) {
            return Redirect::to("/home");
        }
        else {
            Session::flash("registration_error", "Error occurred. Please try again.");
            return Redirect::to("/");
        }
    }

    public function update() {
        $user = Auth::user();

        return view("update_profile", compact("user"));
    }

    public function upload(Request $request) {
        $date  = str_replace(array(" ", ":"), "", date('Y-m-d H:i:s'));

        $image = $request->image;
        $img_arr_1 = explode(";", $image);
        $img_arr_2 = explode(",", $img_arr_1[1]);
        $image = base64_decode($img_arr_2[1]);

        $imageName = "images/user_images/" . $date . ".jpg";
        file_put_contents($imageName, $image);

        $user = User::find(Auth::user()->id);
        $user->image = $imageName;
        $user->save();

        return $user->image;
    }

    public function saveCredentials(Request $request) {
        $user = User::find(Auth::user()->id);
        $newUsername = $request->username;
        $currUsername = $user->username;
        $newPassword = $request->password;

        if ($newUsername != $currUsername) {
            $user->username = $newUsername;
        }

        if ($newPassword != "") {
            $user->password = Hash::make($newPassword);
        }

        $user->save();

        return response()->json(["result" => "Changes have been saved."]);
    }

    public function saveProfile(Request $request) {
        $user = User::find(Auth::user()->id);
        $user->last_name = ucwords($request->last_name);
        $user->first_name = ucwords($request->first_name);
        $user->birthday = $request->year . "-" . $request->month . "-" . $request->day;
        $user->save();

        return response()->json(["result" => "Changes have been saved."]);
    }

    public function saveTheme(Request $request) {
        $user = User::find(Auth::user()->id);
        $user->theme_id = $request->theme;
        $user->save();
    }

    public function login(Request $request) {
        $userdata = array(
            "username" => $request->login_username,
            "password" => $request->login_password
        );

        if (Auth::attempt($userdata)) {
            return Redirect::to("/home");
        }
        else {
            Session::flash("invalidCredentials", "Incorrect username / password");
            return Redirect::to("/")
                    ->withInput(Input::except("password"));
        }
    }

    public function logout() {
        Auth::logout();
        return Redirect::to("/");
    }

    public function profile($username) {
        $user = User::where("username", $username)->get()->first();
        $posts = $user->posts()->orderBy("created_at", "desc")->get();

        return view("user_profile", compact("user", "posts"));
    }
}
