<?php

namespace App\Http\Controllers;

use Auth;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index() {
        $posts = Post::orderBy("created_at", "desc")->get();

        return view("home", compact("posts"));
    }

    public function create(Request $request) {
        $newPost = new Post();
        $newPost->title = $request->title;
        $newPost->body = nl2br($request->body);
        $newPost->user_id = Auth::user()->id;
        $newPost->save();

        return $newPost->id;
    }

    public function update(Request $request) {
        $post = Post::find($request->id);
        $post->title = $request->title;
        $post->body = nl2br($request->body);
        $post->save();
    }

    public function delete(Request $request) {
        $post = Post::find($request->id);
        $post->delete();
    }
}
