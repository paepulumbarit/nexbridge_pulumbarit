<?php

namespace App\Http\Controllers;

use Auth;
use App\Theme;
use Illuminate\Http\Request;

class ThemeController extends Controller
{
    public function index() {
        $themes = Theme::all();
        $user_theme = Auth::user()->theme_id;
        $data = array($themes, $user_theme);

        return $data;
    }
}
