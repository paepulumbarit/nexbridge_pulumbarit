<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/user/find', 'UserController@find');

Route::middleware("guest")->group(function() {
    Route::get('/', function () {
        return view('welcome');
    })->name("login");
    Route::post('/login', 'UserController@login');
    Route::get('/register', function() {
        return view("additional-registration-form");
    });
    Route::post('/register', 'UserController@create');
});

Route::middleware("auth")->group(function() {
    Route::get('/home', 'PostController@index');
    Route::get('/logout', 'UserController@logout');
    Route::get('/update', 'UserController@update');
    Route::get('/themes', 'ThemeController@index');
    Route::put('/update/credentials', 'UserController@saveCredentials');
    Route::put('/update/profile', 'UserController@saveProfile');
    Route::put('/update/theme', 'UserController@saveTheme');
    Route::put('/upload', 'UserController@upload');
    Route::post('/post', 'PostController@create');
    Route::put('/post', 'PostController@update');
    Route::delete('/post', 'PostController@delete');

    Route::get('/{username}', 'UserController@profile');
});
