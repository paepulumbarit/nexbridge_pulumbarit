<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('themes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('image');
        });

        DB::table('themes')->insert(
            array(
                [
                    'name' => 'Nature',
                    'image' => 'images/themes_images/nature.jpg'
                ],
                [
                    'name' => 'Polygon',
                    'image' => 'images/themes_images/polygon.jpg'
                ],
                [
                    'name' => 'Techie',
                    'image' => 'images/themes_images/techie.jpg'
                ],
                [
                    'name' => 'Minimalist',
                    'image' => 'images/themes_images/minimalist.jpg'
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('themes');
    }
}
