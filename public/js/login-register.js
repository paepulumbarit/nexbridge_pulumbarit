let hasAttemptedLogin = false;
const loginFields = document.querySelectorAll(".login-field");

const regFields = document.querySelectorAll(".reg-field");
const regUsername = document.querySelector("#txt-reg-user");
const regPass = document.querySelector("#txt-reg-pass");
const regConfPass = document.querySelector("#txt-reg-conf-pass");
const passWarning = regPass.nextElementSibling;
const cPassWarning = regConfPass.nextElementSibling;

const csrf_token = document.querySelector("meta[name='csrf-token']").getAttribute("content");

const validateLoginFields = function(field) {
    if (field.type != "password") {
        field.value = field.value.trim();
    }

    if (0 == field.value.length) {
        field.nextElementSibling.classList.remove("d-none");
    }
}

const showRegWarning = function(warningObj, msg) {
    if ("" != msg) {
        warningObj.innerHTML = msg;
        warningObj.classList.remove("d-none");
    }
}

const removeWarning = function(warningObj) {
    if (!warningObj.classList.contains("d-none")) {
        warningObj.classList.add("d-none");
    }
}

const haveInvalidField = function(fields) {
    let haveInvalidField = false;
    fields.forEach(function(field) {
        if (!field.nextElementSibling.classList.contains("d-none")) {
            haveInvalidField = true;
        }
    });

    return haveInvalidField;
}

const checkUsernameAvailability = async function(usernameWarning, username) {
    let data = new FormData();
    data.append("_token", csrf_token);
    data.append("username", username);

    let warningMsg = "";

    await fetch("/user/find", {
        method: "POST",
        body: data
    })
    .then(function(res) {
        return res.text();
    })
    .then(function(data) {
        if (0 < data) {
            warningMsg = "Username already exists!";
        }
        else {
            removeWarning(usernameWarning)
        }
    });

    return warningMsg;
}

loginFields.forEach(function(field) {
    field.addEventListener("focus", function() {
        removeWarning(this.nextElementSibling);
    });

    field.addEventListener("blur", function() {
        validateLoginFields(this);
    });
});

document.querySelector("#btn-login").addEventListener("click", function(event) {
    loginFields.forEach(function(field) {
        validateLoginFields(field);
    });

    const hasInvalidFields = haveInvalidField(loginFields)

    if (hasInvalidFields) {
        event.preventDefault();
    }
});

document.querySelector("#btn-reg").addEventListener("click", async function() {
    regFields.forEach(function(field) {
        if (field.type != "password") {
            field.value = field.value.trim();
        }

        if (0 == field.value.length) {
            field.nextElementSibling.classList.remove("d-none");
            field.nextElementSibling.innerHTML = "This field is required.";
        }
    });

    let hasInvalidFields = haveInvalidField(regFields) || (await(checkUsernameAvailability(regUsername.nextElementSibling, regUsername.value)) != "");

    if (hasInvalidFields) {
        return;
    }

    await fetch("/register", {
        method: "GET"
    })
    .then(function(res) {
        return res.text();
    })
    .then(function(data) {
        $("#modalTitle").text("Register");
        $(".modal-body").empty();
        $(".modal-body").append(data);
        $("#modalTemplate").modal("show");
    });

    document.querySelector("#txtUsername").value = regUsername.value;
    document.querySelector("#txtPassword").value = regPass.value;
});

regFields.forEach(function(field) {
    const warningObj = field.nextElementSibling;

    field.addEventListener("focus", function() {
        removeWarning(warningObj)
    });
});

regUsername.addEventListener("blur", async function() {
    const username = this.value.trim();
    const usernameWarning = regUsername.nextElementSibling;
    let warningMsg = "";

    if (0 < username.length) {
        if (username.length < 8 || username.length > 14) {
            warningMsg = "Username should be 8-14 characters.";
        }
        else {
            if (username.match(/[^0-9a-z]/i)) {
                warningMsg = "Only letters and digits allowed for username";
            }
            else {
                warningMsg = await checkUsernameAvailability(usernameWarning, username);
            }
        }
    }
    else {
        warningMsg = "This field is required.";
    }

    showRegWarning(usernameWarning, warningMsg);
});

regPass.addEventListener("blur", function() {
    const pass = this.value;
    const confPass = regConfPass.value;

    let warningMsg = "";

    if (confPass.length > 0 && confPass != pass) {
        cPassWarning.innerHTML = "Passwords do not match.";
        cPassWarning.classList.remove("d-none");
    }
    else {
        removeWarning(cPassWarning);
    }

    if (pass.length > 0) {
        if (pass.length < 8) {
            warningMsg = "Password must be at least 8 characters.";
        }
        else {
            if (!pass.match(/\d/)) {
                warningMsg = "Password must contain at least 1 number.";
            }
        }
    }
    else {
        warningMsg = "This field is required.";
    }

    showRegWarning(passWarning, warningMsg)
});

regConfPass.addEventListener("blur", function() {
    const pass = regPass.value;
    const confPass = this.value;
    let warningMsg = "";

    if (0 < confPass.length) {
        if (pass != confPass) {
            warningMsg = "Passwords do not match.";
        }
    }
    else {
        warningMsg = "This field is required.";
    }

    showRegWarning(cPassWarning, warningMsg);
});
