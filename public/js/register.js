var selectMonth = document.querySelector("#selectMonth");
var selectDay = document.querySelector("#selectDay");
var nameFields = document.querySelectorAll(".txt-name");

var changeDay = function() {
    const maxNoOfDays = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    const selectedMonth = selectMonth.value;
    selectDay.innerHTML = "";

    for (let i = 1; i <= maxNoOfDays[parseInt(selectedMonth) - 1]; i++) {
        let day = document.createElement("option");
        day.value = day.innerHTML = i;
        selectDay.add(day);
    }
}

var validateName = function(field) {
    const input = field.value = field.value.trim();
    let warningMsg = "";

    if (0 == input.length) {
        warningMsg = "This field is required.";
    }
    else {
        if (input.match(/\d/)) {
            warningMsg = "Invalid name.";
        }
    }

    if ("" != warningMsg) {
        field.nextElementSibling.innerHTML = warningMsg;
        field.nextElementSibling.classList.remove("d-none");
    }
}

selectMonth.addEventListener("change", function() {
    changeDay();
});

nameFields.forEach(function(field) {
    field.addEventListener("blur", function() {
        validateName(field);
    })

    field.addEventListener("focus", function() {
        removeWarning(field.nextElementSibling)
    })
})

document.querySelector("#btnReg").addEventListener("click", function(event) {
    let hasInvalidField = false;

    nameFields.forEach(function(field) {
        validateName(field);
        if (!field.nextElementSibling.classList.contains("d-none")) {
            hasInvalidField = true;
        }
    })

    if (hasInvalidField) {
        event.preventDefault();
    }
})

changeDay();