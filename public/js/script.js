const post_inputs = document.querySelectorAll(".post-inputs");
const csrf_token = document.querySelector("meta[name='csrf-token']").getAttribute("content");
const btnPost = document.querySelector("#btnPost");
const selectMonth = document.querySelector("#selectMonth");
const selectDay = document.querySelector("#selectDay");
const modalTitle = document.querySelector("#modalTitle");
const modalBody = document.querySelector(".modal-body");
const profilePicDiv = document.querySelector("#avatarDiv");
const changeThemeLink = document.querySelector("#change-theme-btn");
let croppieObj;

let postTitle, postBody, postToDelete;

const postEntry = async function(title, body) {
    let data = new FormData(document.querySelector("#form-new-post"));
    let newPostData;

    await fetch("/post", {
        method: "post",
        body: data
    })
    .then(function(res) {
        return res.text();
    })
    .then(function(data) {
        newPostId = data;
    })

    return newPostId;
}

const enableTooltip = function() {
    $("[data-toggle='tooltip']").tooltip();
}

const changeDay = function() {
    const maxNoOfDays = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    const selectedMonth = selectMonth.value;
    selectDay.innerHTML = "";

    for (let i = 1; i <= maxNoOfDays[parseInt(selectedMonth) - 1]; i++) {
        let day = document.createElement("option");
        day.value = day.innerHTML = i;
        selectDay.add(day);
    }
}

const disableButtonWhileProcessing = function(obj, msg) {
    obj.setAttribute("disabled", true);
    obj.innerHTML = msg;
}

const enableAfterProcessing = function(obj, msg) {
    obj.removeAttribute("disabled");
    obj.innerHTML = msg;
}

post_inputs.forEach(function(field) {
    field.addEventListener("blur", function() {
        this.value = this.value.trim();
    })

    field.addEventListener("focus", function() {
        if (this.classList.contains("border-danger")) {
            this.classList.remove("border");
            this.classList.remove("border-danger");
        }
    })
});

if(btnPost) {
    btnPost.addEventListener("click", async function() {
        let haveEmptyField = false;

        post_inputs.forEach(function(field) {
            if (0 == field.value.length) {
                field.classList.add("border");
                field.classList.add("border-danger");
                haveEmptyField = true;
            }
        })

        if (!haveEmptyField) {
            const origVal = this.innerHTML;
            disableButtonWhileProcessing(this, "Posting...");

            const id = await postEntry(txtTitle.value, txtBody.value);
            enableAfterProcessing(this, origVal);

            let postsContainer = document.querySelector("#posts-container");

            let olderPosts = document.querySelectorAll(".post");
            postsContainer.innerHTML = "";

            let divRow = document.createElement("div");
            divRow.classList.add("row");

            let divTitleBtns = document.createElement("div");
            divTitleBtns.classList.add("col-10");

            let newPostTitle = document.createElement("h4");
            newPostTitle.classList.add("post-titles");
            newPostTitle.innerHTML = txtTitle.value;

            let btnEditDiv = document.createElement("div");
            btnEditDiv.classList.add("col-1");
            btnEditDiv.classList.add("text-warning");
            btnEditDiv.classList.add("text-right");

            let btnEdit = document.createElement("i");
            btnEdit.classList.add("fas");
            btnEdit.classList.add("fa-pencil-alt");
            btnEdit.classList.add("fa-2x");
            btnEdit.classList.add("post-options");
            btnEdit.classList.add("edit-post-btn");
            btnEdit.setAttribute("data-toggle", "tooltip");
            btnEdit.setAttribute("data-id", id);
            btnEdit.setAttribute("data-placement", "top");
            btnEdit.setAttribute("title", "Edit rant");

            btnEditDiv.appendChild(btnEdit);

            let btnDelDiv = document.createElement("div");
            btnDelDiv.classList.add("col-1");
            btnDelDiv.classList.add("text-danger");
            btnDelDiv.classList.add("text-left");

            let btnDel = document.createElement("i");
            btnDel.classList.add("fas");
            btnDel.classList.add("fa-times");
            btnDel.classList.add("fa-2x");
            btnDel.classList.add("post-options");
            btnDel.classList.add("del-post-btn");
            btnDel.setAttribute("data-toggle", "tooltip");
            btnDel.setAttribute("data-id", id);
            btnDel.setAttribute("data-placement", "top");
            btnDel.setAttribute("title", "Delete rant");

            btnDelDiv.appendChild(btnDel);

            divTitleBtns.appendChild(newPostTitle);

            let newPostBody = document.createElement("p");
            newPostBody.classList.add("text-justify");
            newPostBody.classList.add("post-bodies");
            newPostBody.classList.add("p-2");
            newPostBody.innerHTML = txtBody.value.replace(/\n\r?/g, '<br>');

            let newPostDate = document.createElement("h6");
            newPostDate.classList.add("text-secondary");
            newPostDate.classList.add("p-2");
            newPostDate.innerHTML = Date.today().toString("MMMM dd, yyyy") + " | Just now";

            divRow.appendChild(divTitleBtns)
            divRow.appendChild(btnEditDiv);
            divRow.appendChild(btnDelDiv);

            let bodyDiv = document.createElement("div");
            bodyDiv.classList.add("col-12");

            bodyDiv.appendChild(newPostBody);
            bodyDiv.appendChild(newPostDate);
            bodyDiv.appendChild(newPostDate);

            divRow.appendChild(bodyDiv);

            let newPost = document.createElement("div");
            newPost.classList.add("w-100");
            newPost.classList.add("bg-white");
            newPost.classList.add("border");
            newPost.classList.add("border-secondary");
            newPost.classList.add("p-4");
            newPost.classList.add("my-3");
            newPost.classList.add("post");

            newPost.appendChild(divRow);

            postsContainer.appendChild(newPost);

            olderPosts.forEach(function(post) {
                postsContainer.appendChild(post);
            });

            txtTitle.value = txtBody.value = "";
            enableTooltip();
        }
    })
}

if (changeThemeLink) {
    changeThemeLink.addEventListener("click", function() {
        modalTitle.innerHTML = "Change Theme";
        modalBody.innerHTML = "";

        fetch("/themes", {
            method: "get"
        })
        .then(function(res) {
            return res.text();
        })
        .then(function(data) {
            const parsedData = JSON.parse(data);
            const themes = parsedData[0];
            const userCurrTheme = parsedData[1];
            const themeForm = document.createElement("form");
            const selectTheme = document.createElement("select");

            themeForm.action = "";

            selectTheme.classList.add("form-control");
            selectTheme.classList.add("custom-select");
            selectTheme.id = "themeSelector";

            themes.forEach(function(theme) {
                const themeOption = document.createElement("option");
                const themeId = theme["id"];

                themeOption.value = themeId;
                themeOption.innerHTML = theme["name"];
                selectTheme.appendChild(themeOption);

                if (themeId == userCurrTheme) {
                    selectTheme.value = themeId;
                }
            })

            modalBody.appendChild(selectTheme);

            themes.forEach(function(theme) {
                console.log(theme["name"])
            })
            $("#modalTemplate").modal("show");
        });
    })

    $(document).on("change", "#themeSelector", function() {
        $(this).attr("disabled", true);
        let formData = new FormData();
        formData.append("_method", "put");
        formData.append("_token", csrf_token);
        formData.append("theme", $(this).val());

        fetch("/update/theme", {
            method: "post",
            body: formData
        })
        .then(function(res) {
            return res.text();
        })
        .then(function(data) {
            window.location.reload();
        })
    });
}

$("#file-input").on("change", function() {
    modalTitle.innerHTML = "Change Avatar";
    modalBody.innerHTML = "";

    let croppieDiv = document.createElement("div");
    croppieDiv.id = "image_demo";
    croppieDiv.classList.add("mx-auto");
    croppieDiv.classList.add("w-100");

    let btnDiv = document.createElement("div");
    btnDiv.classList.add("w-100");
    btnDiv.classList.add("text-center")

    let uploadBtn = document.createElement("button");
    uploadBtn.classList.add("btn");
    uploadBtn.classList.add("btn-success");
    uploadBtn.classList.add("crop_image");
    uploadBtn.innerHTML = "Save avatar";

    btnDiv.appendChild(uploadBtn);

    modalBody.appendChild(croppieDiv);
    modalBody.appendChild(btnDiv);

    croppieObj = $image_crop = $("#image_demo").croppie({
        enableExif: true,
        viewport: {
            width: 200,
            height: 200,
            type: "square"
        },
        boundary: {
            width: 300,
            height: 300
        }
    })

    let reader = new FileReader();
    reader.onload = function(event) {
        $image_crop.croppie("bind", {
            url: event.target.result
        })
        .then(function() {
            console.log("jQuery bind complete.");
        })
    }
    reader.readAsDataURL(this.files[0]);
    $("#modalTemplate").modal("show");
})

$(document).on("click", ".crop_image", function() {
    $(this).attr("disabled", true);
    $(this).text("Saving...")
    croppieObj.croppie("result", {
        type: "canvas",
        size: "viewport"
    })
    .then(async function(res) {
        let formData = new FormData();
        formData.append("image", res);
        formData.append("_token", csrf_token);
        formData.append("_method", "put");

        await fetch("/upload", {
            method: "post",
            body: formData
        })
        .then(function(res) {
            return res.text();
        })
        .then(function(data) {
            location.reload();
        })
    })
})

$("#file-input").on("click", function() {
    $(this).val("");
})

$(document).on("click", ".edit-post-btn", function() {
    postTitle = $(this).parent().prev().find("h4");
    postBody = $(this).parent().next().next().find("p");

    let editForm = document.createElement("form");
    editForm.id = "edit-form";

    let id = document.createElement("input")
    id.type = "hidden";
    id.value = $(this).attr("data-id");
    id.name = "id";

    let txtTitle = document.createElement("input");
    txtTitle.type = "text";
    txtTitle.id = "txtEditTitle";
    txtTitle.classList.add("form-control");
    txtTitle.classList.add("mb-4");
    txtTitle.classList.add("edit-post-inputs");
    txtTitle.value = postTitle.text();
    txtTitle.name = "title";

    let txtBody = document.createElement("textarea");
    txtBody.id = "txtEditBody";
    txtBody.classList.add("form-control")
    txtBody.classList.add("edit-post-inputs");
    txtBody.rows = 4;
    txtBody.name = "body";
    txtBody.innerHTML = postBody.text();

    let buttonDiv = document.createElement("div");
    buttonDiv.classList.add("w-100");
    buttonDiv.classList.add("text-right");

    let saveBtn = document.createElement("button");
    saveBtn.classList.add("btn");
    saveBtn.classList.add("btn-success");
    saveBtn.classList.add("px-4");
    saveBtn.classList.add("mt-2");
    saveBtn.type = "button";
    saveBtn.id = "saveChanges";
    saveBtn.innerHTML = "<i class='fas fa-save'></i> Save Changes";

    buttonDiv.appendChild(saveBtn);

    editForm.appendChild(id);
    editForm.appendChild(txtTitle);
    editForm.appendChild(txtBody);
    editForm.appendChild(buttonDiv);

    modalTitle.innerHTML = "Edit Rant";
    modalBody.innerHTML = "";
    modalBody.appendChild(editForm);

    $("#modalTemplate").modal("show");
})

$(document).on("click", ".del-post-btn", function() {
    postToDelete = $(this).parent().parent().parent().index();

    let confirmMsg = document.createElement("p");
    confirmMsg.innerHTML = "Are you sure you want to delete this rant?";

    let divButtons = document.createElement("div");
    divButtons.classList.add("w-100")
    divButtons.classList.add("text-right");
    divButtons.classList.add("py-3");

    let confirmBtn = document.createElement("button");
    confirmBtn.id = "btnConfirmDelete";
    confirmBtn.classList.add("btn");
    confirmBtn.classList.add("btn-success");
    confirmBtn.classList.add("w-25");
    confirmBtn.setAttribute("data-id", $(this).attr("data-id"));
    confirmBtn.innerHTML = "Yes";

    let cancelBtn = document.createElement("button");
    cancelBtn.id = "btnCancel"
    cancelBtn.classList.add("btn");
    cancelBtn.classList.add("btn-secondary");
    cancelBtn.classList.add("w-25");
    cancelBtn.classList.add("ml-2");
    cancelBtn.innerHTML = "Cancel";

    divButtons.appendChild(confirmBtn);
    divButtons.appendChild(cancelBtn);

    modalTitle.innerHTML = "Confirm deletion";

    modalBody.innerHTML = "";
    modalBody.appendChild(confirmMsg);
    modalBody.appendChild(divButtons);

    $("#modalTemplate").modal("show");
})

$(document).on("click", "#saveChanges", async function() {
    let haveEmptyField = false;
    $(this).attr("disabled", true);
    $(this).text("Saving...");

    document.querySelectorAll(".edit-post-inputs").forEach(function(field) {
        if (0 == field.value.length) {
            field.classList.add("border");
            field.classList.add("border-danger");
            haveEmptyField = true;
        }
    });

    let formData = new FormData(document.querySelector("#edit-form"));
    formData.append("_token", csrf_token);
    formData.append("_method", "put");

    if (!haveEmptyField) {
        await fetch("/post", {
            method: "post",
            body: formData
        })
        .then(function(res) {
            return res.text();
        })
        .then(function(data) {
            postTitle.html(document.querySelector("#txtEditTitle").value);
            postBody.html(document.querySelector("#txtEditBody").value.replace(/\n\r?/g, '<br>'));
            $("#modalTemplate").modal("hide");
        })
    }
})

$(document).on("blur", ".edit-post-inputs", function() {
    $(this).val($(this).val().trim());
})

$(document).on("focus", ".edit-post-inputs", function() {
    if ($(this).hasClass("border-danger")) {
        $(this).removeClass("border");
        $(this).removeClass("border-danger");
    }
})

$(document).on("click", "#btnCancel", function() {
    $("#modalTemplate").modal("hide");
})

$(document).on("click", "#btnConfirmDelete", async function() {
    $(this).attr("disabled", true);
    $(this).text("Deleting...");

    let formData = new FormData();
    formData.append("id", $(this).attr("data-id"));
    formData.append("_method", "delete");
    formData.append("_token", csrf_token);

    await fetch("/post", {
        method: "post",
        body: formData
    })
    .then(function(res) {
        return res.text();
    })
    .then(function(data) {
        let postsContainer = $("#posts-container");

        let olderPosts = $(".post");
        postsContainer.text("");

        if (1 == olderPosts.length)
        {
            if ("/home" == window.location.pathname) {
                postsContainer.append("<p id='no-posts'>Great, no one is ranting!</p>");
            }
            else {
                postsContainer.append("<p id='no-posts'>You haven't ranted yet!</p>");
            }
        }
        else {
            $.each(olderPosts, function(post) {
                if (post != postToDelete) {
                    postsContainer.append($(this).get());
                }
            });
        }

        $("#modalTemplate").modal("hide");
    })
})

if ("/update" == window.location.pathname) {
    const usernameField = document.querySelector("#txtUsername");
    const passwordField = document.querySelector("#txtPassword");
    const credWarning = document.querySelector("#cred-warning");
    const profWarning = document.querySelector("#prof-warning");
    const lastNameField = document.querySelector("#txtLastName");
    const firstNameField = document.querySelector("#txtFirstName");
    const monthField = document.querySelector("#selectMonth");
    const dayField = document.querySelector("#selectDay");
    const yearField = document.querySelector("#selectYear");

    selectMonth.addEventListener("change", function() {
        changeDay();
    });

    selectMonth.value = selectMonth.getAttribute("data-month");
    changeDay();
    selectDay.value = selectDay.getAttribute("data-day");

    let currUsername = usernameField.value;
    const currLastName = lastNameField.value;
    const currFirstName = firstNameField.value;
    const currMonth = monthField.value;
    const currDay = dayField.value;
    const currYear = yearField.value;

    const addWarning = function(field, msg) {
        field.classList.add("border");
        field.classList.add("border-danger");
        field.nextElementSibling.classList.remove("d-none");
        field.nextElementSibling.innerHTML = msg;

        return true
    }

    const checkUsernameAvailability = async function(username) {
        let data = new FormData();
        data.append("_token", csrf_token);
        data.append("username", username);

        let usernameExists = false;

        await fetch("/user/find", {
            method: "post",
            body: data
        })
        .then(function(res) {
            return res.text();
        })
        .then(function(data) {
            if (0 < data) {
                usernameExists = true;
            }
        });

        return usernameExists;
    }

    const saveCredChanges = async function() {
        if (txtUsername.value == currUsername && txtPassword.value == "") {
            credWarning.classList.remove("d-none");
            return;
        }

        let formData = new FormData(document.querySelector("#login-cred-form"));
        formData.append("_method", "put")

        await fetch("/update/credentials", {
            method: "post",
            body: formData
        })
        .then(function(res) {
            return res.text();
        })
        .then(function(data) {
            modalTitle.innerHTML = "Success";
            modalBody.innerHTML = "";
            modalBody.append(JSON.parse(data)["result"])
            $("#modalTemplate").modal("show");
            currUsername = formData.get("username");

            console.log("wgd")
            document.querySelector("#profileLink").setAttribute("href", usernameField.value);
        })
    }

    document.querySelector("#saveCredChanges").addEventListener("click", async function(e) {
        let haveInvalidField = false;
        let confPassField = document.querySelector("#txtConfPass");
        let userNameValue = usernameField.value = usernameField.value.trim();

        if (0 == userNameValue.trim().length) {
            haveInvalidField = addWarning(usernameField, "This field is required.");
        }
        else if (userNameValue.length < 8 || userNameValue.length > 14) {
            haveInvalidField = addWarning(usernameField, "Username should be 8-14 characters.");
        }
        else if (userNameValue.match(/[^0-9a-z]/i)) {
            haveInvalidField = addWarning(usernameField, "Only letters and digits allowed for username");
        }
        else {
            if (currUsername != userNameValue) {
                haveInvalidField = await(checkUsernameAvailability(userNameValue));
                if (haveInvalidField) {
                    addWarning(usernameField, "Username already exists!");
                }
            }
        }

        if (0 < passwordField.value.length) {
            if (8 > passwordField.value.length) {
                haveInvalidField = addWarning(passwordField, "Password must be at least 8 characters.");
            }
            else if (!passwordField.value.match(/\d/)) {
                haveInvalidField = addWarning(passwordField, "Password must contain at least 1 number.");
            }
            else {
                const confPassValue = confPassField.value;
                if (0 == confPassValue.length) {
                    haveInvalidField = addWarning(confPassField, "This field is required.");
                }
                else {
                    if (confPassValue != passwordField.value) {
                        haveInvalidField = addWarning(confPassField, "Passwords do not match");
                    }
                }
            }
        }

        if (haveInvalidField) {
            return;
        }

        saveCredChanges();
    });

    document.querySelector("#saveProfileChanges").addEventListener("click", async function() {
        let haveInvalidField = false;
        const newLastName = lastNameField.value = lastNameField.value.trim();
        const newFirstName = firstNameField.value = firstNameField.value.trim();
        const newMonth = monthField.value;
        const newDay = dayField.value;
        const newYear = yearField.value;

        document.querySelectorAll(".name-field").forEach(function(field) {
            const input = field.value;

            if (0 == input.length) {
                switch (field.getAttribute("name")) {
                    case "last_name":
                        field.value = currLastName;
                        break;
                    default:
                        field.value = currFirstName;
                        break;
                }
            }
            else {
                if (field.value.match(/\d/)) {
                    haveInvalidField = addWarning(field, "Invalid name.");
                }
            }
        })

        if (haveInvalidField) {
            return;
        }

        if (newLastName.toLowerCase() == currLastName.toLowerCase() &&
            newFirstName.toLowerCase() == currFirstName.toLowerCase() &&
            newMonth == currMonth && newDay == currDay && newYear == currYear) {
            profWarning.classList.remove("d-none");
            return;
        }

        let formData = new FormData(document.querySelector("#profile-form"))
        formData.append("_method", "put");

        await fetch("/update/profile", {
            method: "post",
            body: formData
        })
        .then(function(res) {
            return res.text();
        })
        .then(function(data) {
            modalTitle.innerHTML = "Success";
            modalBody.innerHTML = "";
            modalBody.append(JSON.parse(data)["result"])
            $("#modalTemplate").modal("show");
        })
    })

    document.querySelectorAll("input").forEach(function(field) {
        field.addEventListener("focus", function() {
            const warningField = this.nextElementSibling;

            if (this.type == "button") {
                return;
            }

            if (!warningField.classList.contains("d-none")) {
                warningField.classList.add("d-none")
                this.classList.remove("border")
                this.classList.remove("border-danger")
            }

            if (field.classList.contains("cred-field") && !credWarning.classList.contains("d-none")) {
                credWarning.classList.add("d-none");
            }

            if (field.classList.contains("name-field") && !profWarning.classList.contains("d-none")) {
                profWarning.classList.add("d-none");
            }
        })
    });

    document.querySelectorAll("select").forEach(function(obj) {
            obj.addEventListener("change", function() {
            if (!profWarning.classList.contains("d-none")) {
                profWarning.classList.add("d-none");
            }
        })
    })
}

enableTooltip();
